﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Web.Http;
using Blog.Models;

namespace Blog.Controllers
{
    public class BlogController : ApiController
    {
        //Blogs are retrieved from the Database. However due to error in connection, I cannot retrieve them. 
        // BlogController blg = new BlogController();
        // Blogs blogs = blg.blogs.ToList();




        Blogs[] blogs = new Blogs[]{
        new Blogs { ID = 1, Title = "Nepal",  Description = "Nepal is most beautiful contry ", Keyword = "Nepal", PublishedDate =
            DateTime.Parse(DateTime.Today.ToString()), AuthorId = 5 },
         new Blogs { ID = 1, Title = "Nepal",  Description = "Nepal is most beautiful contry in world.", Keyword = "Nepal", PublishedDate =
            DateTime.Parse(DateTime.Today.ToString()), AuthorId = 6 },
            
      };
        public IEnumerable<Blogs> GetAllBlogs()
        {
            return blogs;
        }

        public IHttpActionResult GetBlog(int id)
        {
            var blog = blogs.FirstOrDefault((p) => p.ID == id);
            if (blog == null)
            {
                return NotFound();
            }
            return Ok(blog);
        }
    }
}