﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



namespace Blog.Models
{
    public class Blogs
    {
        //Database with table named Author and Blog are made.
        //Author Contains:
        // [Id] INT NOT NULL,
        // [Name] VARCHAR(50) NOT NULL
        // Blog Contains:
        // [ID] INT NOT NULL,
        // [Title] VARCHAR(50) NOT NULL,
        // [Description]   VARCHAR(50) NOT NULL,
        // [Keyword]       VARCHAR(50) NOT NULL,
        // [PublishedDate] DATETIME NOT NULL,
        // [AuthorId] 
        //   INT NOT NULL,

        public int ID { get; set; }
        public string Title { get; set; }
        public String Description { get; set; }
        public String Keyword { get; set; }
        public DateTime PublishedDate { get; set; }
        public int AuthorId { get; set; }

    }
}